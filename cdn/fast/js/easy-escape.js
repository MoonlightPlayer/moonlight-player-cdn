var siteName = {{ mySite }};
var tagLine = {{ myTag }};
var tagName = {{ mySite }} | {{ myTag }};

var app = angular.module('CPanl', []);
app.controller('CtrlPanel', function($scope) {
    $scope.mySite = "{{ mySite }}";
    $scope.myTag = "{{ myTag }}";
});